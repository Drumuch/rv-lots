$(document).ready(function(){
    // CLICK ON MENU BUTTON *************************************************************
    $('.head_menu_button').click(function() {
        if ( $( '.head_menu_button' ).hasClass("active") ) {
            $(".head_menu_onclick").removeClass('menu_active');
            $('.head_menu_button').removeClass("active");
            console.log("remove");
        } else {
            $(".head_menu_onclick").addClass('menu_active');
            $('.head_menu_button').addClass("active");
            console.log("add");
        }
    });
    // CLICK ON MENU BUTTON *************************************************************
    $('.fixedHead_side_bar').click(function() {
        if ($(this).hasClass('activeB')) {
            $(this).css({
                left: "-245px",
                height: "100px"
            })
                .removeClass('activeB');
            $('.close_button').css({
                top: "42px",
                left: "0px"
            })
                .removeClass('fa-times')
                .addClass('fa-arrow-right');
        } else {
            $(this).css({
                left: "0px",
                height: "183px"
            })
                .addClass('activeB');
            $('.close_button').css({
                top: "0px",
                left: "-10px"
            })
                .removeClass('fa-arrow-right')
                .addClass('fa-times');
        }
    });
    (window.onresize = function(){
      if ($('.head_name_wrap').width() > 760) {
          $(".head_menu_onclick").removeClass('menu_active');
      }
    })()
});

