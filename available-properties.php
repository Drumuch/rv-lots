<?
session_start();
if(!$_SESSION["username"] OR !$_SESSION["password"])
	{
	$visible=" state='1' ";
	}
else
	{
	$visible=" state LIKE '%' ";
	}
?><html>
<head>
<title>RV Lots for Sale Georgia - North Georgia RV Parks - Deeded RV Lots for Sale - Talking Rock RV Resort, located just west of Ellijay GA</title>
<meta name="keywords" content="rv lots for sale georgia, deeded rv lots, georgia mountain rv resort, rv lots georgia, rv georgia, georgia rv parks and campgrounds, rv parks in north georgia, table rock rv, georgia rv lots, rv lots for sale" />
<meta name="description" content="Talking Rock RV Resort offers you your very own deeded RV site, always ready for your getaways and seasonal stays. />
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style>
<!--
table        { font-family: Tahoma; font-size: 11pt; line-height: 150% }
.txtbox      { color: #000000; width: 135; border: 1px solid #C9B993; background-color: 
               #DDD0B2 }
a            { color: #D1DC76; font-family: Verdana; font-size: 10pt; text-decoration: none; 
               font-weight: bold }
a:visited    { color: 83C993; font-family: Verdana; font-size: 10pt; text-decoration: none; 
               font-weight: bold }
a:hover      { color: #F6EFDF }
.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18pt;
	color: #000000;
}
.style8 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11pt;
}
.style9 {color: #83C892}
.style10 {font-family: Arial, Helvetica, sans-serif}
.style15 {font-family: Arial, Helvetica, sans-serif; font-size: 10pt; }
.style17 {
	font-size: 10pt;
	font-weight: bold;
}
.style26 {font-size: 16pt;
	font-style: italic;
	font-family: Georgia, "Times New Roman", Times, serif;
	color: #425834;
}
.style29 {color: #425834;
	font-style: italic;
	font-family: Georgia, "Times New Roman", Times, serif;
	font-size: 16px;
}
.style35 {color: #5a7e41}
.style36 {
	color: #5a7e41;
	font-weight: bold;
	font-family: Arial, Helvetica, sans-serif;
}
a:link {
	color: 83C993;
}
-->
</style>
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>

<script type="text/javascript" src="manage/slide/highslide/highslide-with-gallery.js"></script>
<link rel="stylesheet" type="text/css" href="manage/slide/highslide/highslide.css" />
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="manage/slide/highslide/highslide-ie6.css" />
<![endif]-->
<script type="text/javascript">
hs.graphicsDir = 'manage/slide/highslide/graphics/';
hs.align = 'center';
hs.transitions = ['expand', 'crossfade'];
hs.fadeInOut = true;
hs.outlineType = 'glossy-dark';
hs.wrapperClassName = '';
hs.captionEval = 'this.a.title';
hs.numberPosition = 'caption';
hs.useBox = true;
hs.width = 800;
hs.height = 600;
hs.addSlideshow({
	//slideshowGroup: 'group1',
	interval: 5000,
	repeat: false,
	useControls: true,
	fixedControls: 'fit',
	overlayOptions: {
		position: 'bottom center',
		opacity: .75,
		hideOnMouseOut: true
	},
	thumbstrip: {
		position: 'above',
		mode: 'horizontal',
		relativeTo: 'expander'
	}
});
var miniGalleryOptions1 = {
	thumbnailId: 'thumb1'
}
</script>

</head>
<body bgcolor="#F6EFDF" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" style="background-position: center top; background-repeat:no-repeat" onLoad="MM_preloadImages('images/btnHome_over.jpg','images/btnPropertyListings_over.jpg','images/btnCommunity_over.jpg','images/btnGallery_over.jpg','images/btnContact_over.jpg')">
<div align="center">
	<table border="0" cellpadding="0" style="border-collapse: collapse" width="100%">
		<tr>
			<td background="images/bgTop.jpg" height="510">
			<div align="center">
				<table border="0" cellpadding="0" style="border-collapse: collapse" width="983" height="510">
					<tr>
						<td height="73">
						<div align="center">
							<table border="0" cellpadding="0" style="border-collapse: collapse" width="983" height="117">
								<tr>
									<td height="73">
									<font color="#FFFFFF" size="7">RV-LOTS</font></td>
								</tr>
								<tr>
									<td height="44">
						<img border="0" src="images/logo.jpg" width="324" height="44"></td>
								</tr>
							</table>
						</div>
						</td>
					</tr>
					<tr>
						<td height="35">
						<div align="center">
							<table border="0" cellpadding="0" style="border-collapse: collapse" width="983" height="35">
								<tr>
									<td width="78"><a href="index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image28','','images/btnHome_over.jpg',1)"><img src="images/btnHome.jpg" name="Image28" width="78" height="35" border="0"></a></td>
								  <td width="17">&nbsp;</td>
									<td width="163"><a href="property-listings.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image29','','images/btnPropertyListings_over.jpg',1)"><img src="images/btnPropertyListings.jpg" name="Image29" width="163" height="35" border="0"></a></td>
								  <td width="163"><a href="the-community.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image30','','images/btnCommunity_over.jpg',1)"><img src="images/btnCommunity.jpg" name="Image30" width="163" height="35" border="0"></a></td>
								  <td width="99"><a href="gallery.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image31','','images/btnGallery_over.jpg',1)"><img src="images/btnGallery.jpg" name="Image31" width="99" height="35" border="0"></a></td>
								  <td width="119"><a href="contact-us.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image32','','images/btnContact_over.jpg',1)"><img src="images/btnContact.jpg" name="Image32" width="119" height="35" border="0"></a></td>
								  <td width="344">&nbsp;</td>
								</tr>
							</table>
						</div>
						</td>
					</tr>
					<tr>
						<td height="358" valign="top">
						<div align="center">
							<table border="0" cellpadding="0" style="border-collapse: collapse" width="983" height="358">
								<tr>
									<td width="130" valign="top">
									<img border="0" src="images/bannerLeft.jpg" width="130" height="358"></td>
									<td width="601" valign="top">
									<div align="center">
										<table border="0" cellpadding="0" style="border-collapse: collapse" width="601" height="358">
											<tr>
												<td height="57">
												<img border="0" src="images/bannerTop.jpg" width="601" height="57"></td>
											</tr>
											<tr>
											  <td height="264"><!--<script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','601','height','264','src','banner','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','movie','banner' ); //end AC code
</script><noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="601" height="264">
                                                  <param name="movie" value="banner.swf">
                                                  <param name="quality" value="high">
                                                  <embed src="banner.swf" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="601" height="264"></embed>
											    </object></noscript>--><img border="0" src="images/bannerF.jpg" width="601" height="264"></td>
										  </tr>
											<tr>
												<td height="37">
												<img border="0" src="images/bannerBottom.jpg" width="601" height="37"></td>
											</tr>
										</table>
									</div>
									</td>
									<td width="16" valign="top">
									<img border="0" src="images/bannerRight.jpg" width="17" height="358"></td>
									<td width="235">
									<div align="center">
										<table border="0" cellpadding="0" style="border-collapse: collapse" width="235" height="358">
											<tr>
												<td height="71" colspan="3">&nbsp;</td>
											</tr>
											<tr>
												<td height="137" background="images/wintop.jpg" colspan="3">
												<div align="center">
													<table border="0" cellpadding="0" style="border-collapse: collapse" width="235" height="137">
														<tr>
															<td colspan="3" height="20"></td>
														</tr>
														<tr>
															<td width="17">&nbsp;</td>
															<td width="199" valign="top">
															<p><span class="style26">Welcome to RV Lots</span></p>
															<p align="center"> <span class="style29">Only 5 minutes from <a href="http://carters.sam.usace.army.mil/">Carters Lake </a> Marina</span></p>
															<p align="center">															 
														  <p style="line-height: 1"></td>
															<td width="19">&nbsp;</td>
														</tr>
														<tr>
															<td colspan="3" height="15"></td>
														</tr>
													</table>
												</div>
												</td>
											</tr>
											<tr>
												<td height="113" width="17" rowspan="3">
												<img border="0" src="images/winleft.jpg" width="17" height="113"></td>
												<td height="38" width="200">
												<a href="site-plan.html">
												<img border="0" src="images/siteMap.jpg" width="200" height="38"></a><img border="0" src="images/wincnt.jpg" width="200" height="10"></td>
												<td height="113" width="18" rowspan="3">
												<img border="0" src="images/winright.jpg" width="18" height="113"></td>
											</tr>
											<tr>
												<td height="37" width="200">
												<a href="directions-and-map.html">
												<img border="0" src="images/directions.jpg" width="200" height="37"></a></td>
											</tr>
											<tr>
												<td height="28" width="200">
												<img border="0" src="images/winbottom.jpg" width="200" height="28"></td>
											</tr>
											<tr>
												<td height="37" colspan="3">&nbsp;</td>
											</tr>
										</table>
									</div>
									</td>
								</tr>
							</table>
						</div>
						</td>
					</tr>
				</table>
			</div>
			</td>
		</tr>
		<tr>
			<td height="300" valign="top" align="center">
			<div align="center">
				<table border="0" cellpadding="0" style="border-collapse: collapse" width="983">
					<tr>
						<td width="982" colspan="4" height="40">&nbsp;</td>
					</tr>
					<tr>
						<td width="80">&nbsp;</td>
						<td width="823" valign="top"><h2 class="style10 style35"><strong>Property Listings </strong></h2>						  


<?
include("data.php");
?>










						  <p>
						  <p>&nbsp;</p>
						<p></td>
						<td width="73">&nbsp;</td>
						<td width="6">&nbsp;</td>
					</tr>
					<tr>
						<td width="80">&nbsp;</td>
						<td width="823">&nbsp;</td>
						<td width="73">&nbsp;</td>
						<td width="6">&nbsp;</td>
					</tr>
				</table>
			</div>
			</td>
		</tr>
		<tr>
			<td align="left" height="167">
			<img border="0" src="images/bottomLeft.jpg" width="798" height="169"></td>
		</tr>
		<tr>
			<td bgcolor="#2D221C" height="157" align="left">
			<div align="center">
				<table border="0" cellpadding="0" style="border-collapse: collapse" width="100%" height="157">
					<tr>
						<td align="left" width="22%">
			<img border="0" src="images/bottombottom.jpg" width="34" height="157"></td>
						<td width="62%"><div align="center"><a href="index.php">Home</a> <b>
						<font color="#F6EFDF">|</font></b>&nbsp;
						<a href="property-listings.html">Property Listings</a>&nbsp; 
						<b><font color="#F6EFDF">|</font></b>&nbsp;
						<a href="the-community.php">The Community</a>&nbsp; <b>
						<font color="#F6EFDF">|</font></b>&nbsp;
						<a href="gallery.php">Gallery</a>&nbsp; <b>
						<font color="#F6EFDF">|</font></b>&nbsp;
						<a href="contact-us.html">Contact Us</a></div></td>
						<td width="16%">&nbsp;
						</td>
					</tr>
				</table>
			</div>
			</td>
		</tr>
	</table>
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-4494118-26");
pageTracker._trackPageview();
} catch(err) {}</script></body>
</html>