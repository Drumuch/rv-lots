<head>
    <div class="head_menu_wrap">
        <div class="head_menu">
            <a href="index.php" class="head_menu_home">Home</a>
            <a href="property-listings.php" class="head_menu_listing">Property Listings</a>
            <a href="the-community.php" class="head_menu_community">The Community</a>
            <a href="gallery.php" class="head_menu_gallery">Gallery</a>
            <a href="contact-us.php" class="head_menu_contact">Contact Us</a>
        </div>
    </div>
    <div class="head_slider_bar">
        <div class="head_slider">
            <div class="head_wrap_slider">
                <ul class="bxslider">
                    <li><img src="banner/banner1.jpg" /></li>
                    <li><img src="banner/banner2.jpg" /></li>
                    <li><img src="banner/banner3.jpg" /></li>
                    <li><img src="banner/banner4.jpg" /></li>
                    <li><img src="banner/banner5.jpg" /></li>
                    <li><img src="banner/banner6.jpg" /></li>
                    <li><img src="banner/banner7.jpg" /></li>
                    <li><img src="banner/banner8.jpg" /></li>
                </ul>
            </div>
            <script src="js/slider.js"></script>
            <div class="head_side_underslide">
                <div class="fixed_side_text">Welcome to RV Lots</div>
                <div class="fixed_side_info">&nbsp;Only 5 minutes from <a href="http://carters.sam.usace.army.mil/">Carters Lake </a>  Marina</div>
                <div class="fixed_side_anchor">
                    <a href="site-plan.php" class="head_side_sitemap">SITE MAP
                        <input class="head_side_button" type="button">
                    </a>
                    <a href="directions-and-map.php" class="head_side_directions">DIRECTIONS
                        <input class="head_side_button" type="button">
                    </a>
                </div>
            </div>
            <div class="head_side_bar">
                <div class="head_side_text">Welcome to RV Lots</div>
                <div class="head_side_info">&nbsp;Only 5 minutes from <a href="http://carters.sam.usace.army.mil/">Carters Lake </a>  Marina</div>
                <div class="head_side_anchor">
                    <a href="site-plan.php" class="head_side_sitemap">SITE MAP
                        <input class="head_side_button" type="button"/>
                    </a>
                    <a href="directions-and-map.php" class="head_side_directions">DIRECTIONS
                        <input class="head_side_button" type="button"/>
                    </a>
                </div>
            </div>
        </div>
        <div class="fixedHead_side_bar">
            <div class="head_side_barFixed">
                <div class="fixed_side_text">Welcome to RV Lots</div>
                <div class="fixed_side_info">&nbsp;Only 5 minutes from <a href="http://carters.sam.usace.army.mil/">Carters Lake </a>  Marina</div>
                <div class="fixed_side_anchor">
                    <a href="site-plan.php" class="head_side_sitemap">SITE MAP
                        <input class="head_side_button" type="button"/>
                    </a>
                    <a href="directions-and-map.php" class="head_side_directions">DIRECTIONS
                        <input class="head_side_button" type="button"/>
                    </a>
                </div>
            </div>
            <div class="close_button fa fa-arrow-right"></div>
        </div>
    </div>
</head>
